echo "http://dl-4.alpinelinux.org/alpine/v3.9/main" >> /etc/apk/repositories && \
echo "http://dl-4.alpinelinux.org/alpine/v3.9/community" >> /etc/apk/repositories



apk update && \
libxml2-dev \
libxslt-dev \
curl unzip libexif udev chromium chromium-driver wait4ports xvfb xorg-server dbus ttf-freefont mesa-dri-swrast \
&& rm -rf /var/cache/apk/*