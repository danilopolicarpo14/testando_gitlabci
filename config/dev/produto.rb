# frozen_string_literal: true

PRODUTO = {
  cadastrado: {
    cpf: '16978219010'
  },

  não_cadastrado: {
    cpf: '12345678901'

  },

  conta_ativa: {
    cpf: '12345678999'
  },

  conta_ativa_contestação: {
    cpf: '41472789814'
  },

  sem_tickets: {
    cpf: '61402425252'
  }

}.freeze
