# frozen_string_literal: true

PRODUTO = {
  cadastrado_mais_de_um_produto: {
    cpf: '42487092874'

  },

  cadastrado: {
    cpf: '33160004880'

  },

  não_cadastrado: {
    cpf: '12345678901'

  }

}.freeze
