# frozen_string_literal: true

class Cancel < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/cancel-account'

  def initialize
    @menu_cancelamento_conta = '#menu-item3 > span'
    @cancelamento_conta = '#menu-item3 > ul > li > a'
    @tela_cancelamento_conta = '#content > header > h1'
    @descricao_cancelamento = '#Descrição\ \(opcional\)'
    @produto = '.search-page__results-card-brand'
    @msn_cancelamento = '#content > div.sc-gHboQg.ctfnXu > div.styles__ModalBackground-sc-1kao8jw-0.esGYFx > div > div > div'
  end

  def cancelamento_conta
    find(@menu_cancelamento_conta).hover
    find(@cancelamento_conta).click
  end

  def tela_cancelamento_conta
    find(@tela_cancelamento_conta).text == 'Cancelamento da conta'
  end

  def motivo_cancelamento
    find('label', text: 'Outros').click
    click_button 'Cancelar conta'
  end

  def mensagem_cancelamento
    find_button('OK')
  end

  def motivo_cancelamento_com_descricao
    find('label', text: 'Outros').click
  end

  def informar_descricao
    find(@descricao_cancelamento).set 'testando cancelamento'
    click_button 'Cancelar conta'
  end

  def conta_cancelada
    mensagem_cancelamento
  end

  def buscar_cpf(string)
    first(@cpf).set string
    click_button 'Buscar'
  end

  def produto_cliente
    first(@produto).click
  end

  def cancelar_conta
    click_button 'OK'
    btn_sim
  end

  def msn_cancelamento
    find(@msn_cancelamento)
  end
end
