# frozen_string_literal: true

class Cartao < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/card-list'

  def initialize
    @detalhes_cartoes = '#menu-item2 > ul > li:nth-child(1) > a'
    @page_detalhes = '#content > div:nth-child(2) > header > h1'
    @nome_produto = '#content > div:nth-child(2) > div:nth-child(2) > aside > h4'
    @previsao_entrega = '#content > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(3) > h3:nth-child(3)'
    @previsao_data = '#content > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(3) > h4:nth-child(4)'
    @bandeira = '#content > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(3) > h3:nth-child(1)'
    @bandeira_nome = '#content > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(3) > h4:nth-child(2)'
    @status = '#content > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(3) > h3:nth-child(5)'
    @status_cartao = '#content > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(3) > h4:nth-child(6)'
  end

  def detalhes_cartoes
    find(@detalhes_cartoes).click
  end

  def nome_produto
    find(@nome_produto)
  end

  def page_detalhes
    find(@page_detalhes).text == 'Detalhes de cartões'
  end

  def previsao_entrega
    find(@previsao_entrega)
    find(@previsao_data)
  end

  def bandeira
    find(@bandeira)
    find(@bandeira_nome)
  end

  def status
    find(@status)
    find(@status_cartao)
  end
end
