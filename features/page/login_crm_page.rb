# frozen_string_literal: true

class Login < SitePrism::Page
  set_url 'login'

  def initialize
    @usuario = 'input.InputAutoRefill'
    @senha = 'input.password'
    @mensagem_erro = '#root > div > div.styles__ToastContainer-sc-1fatpcy-0.hKUEaH > div.styles__TextAlert-sc-1fatpcy-1.eCoxHH'
    @tela_buscar = 'input#input-search'
  end

  def realizar_login(arg)
    user = CREDENTIALS[arg.tr(' ', '_').to_sym][:usuario]
    password = CREDENTIALS[arg.tr(' ', '_').to_sym][:senha]
    first(@usuario).set user
    first(@senha).set password
    click_button 'Fazer login'
  end

  def mensagem_erro
    first(@mensagem_erro)
  end

  def tela_buscar
    first(@tela_buscar)
  end
end
