# frozen_string_literal: true

class Tickets < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/tickets'

  def initialize
    @menu_historico = '#menu-item4'
    @historico_atendimento = '#menu-item4 > ul > li > a'
    @tela_tickets = '#root > div.sc-kaNhvL.fnzaYs'
    @msn_tickets = 'Não há tickets'
  end

  def menu_historico
    find(@menu_historico).hover
  end

  def historico_atendimento
    find(@historico_atendimento).click
  end

  def tela_tickets
    find(@tela_tickets, text: 'Tickets de Atendimento')
  end

  def msn_tickets
    find(@tela_tickets, text: @msn_tickets)
  end
end
