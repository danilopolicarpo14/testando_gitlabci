# frozen_string_literal: true

class SegundaVia < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/timeline'

  def initialize
    @cartao = '#menu-item2'
    @segunda_via = '#menu-item2 > ul > li:nth-child(2) > a'
    @tela_segunda_via = '#content > header > h1'
    @msn_bloqueio_cartao = '#block-modal > div.styles__ModalContainer-sc-1kao8jw-1.hiXlsE > div > div > h1'
    @servico_assinatura = '#service-modal > div.styles__ModalContainer-sc-1kao8jw-1.hiXlsE > div > div > h1'
    @previsao_entrega = '#delivery-modal > div.styles__ModalContainer-sc-1kao8jw-1.hiXlsE > div > div > h1'
    @confirmar_emissao = '#confirmation-modal > div > div > div.styles__TitleModal-sc-1kao8jw-5.fiSpyY'
    @btn_sair = '#undo-confirmation'
  end

  def cartao
    find(@cartao).hover
  end

  def segunda_via
    find(@segunda_via).click
  end

  def tela_segunda_via
    find(@tela_segunda_via).text == 'Solicitação de 2ª via'
  end

  def motivo_segunda_via
    find('label', text: 'Cartão Quebrado').click
  end

  def btn_solicitar_segunda_via
    page.execute_script 'window.scrollBy(0,10000)'
    click_button 'Solicitar 2ª via'
  end

  def msn_bloqueio_cartao
    find(@msn_bloqueio_cartao)
  end

  def btn_proximo
    click_button 'Próximo'
  end

  def servico_assinatura
    find(@servico_assinatura)
  end

  def previsao_entrega
    find(@previsao_entrega)
  end

  def confirmar_emissao
    find(@confirmar_emissao)
  end

  def btn_sair
    find(@btn_sair).click
  end

  def btn_sim
    click_button 'Sim'
  end
end
