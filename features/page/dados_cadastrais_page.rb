# frozen_string_literal: true

class Cadastro < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/client-details'

  def initialize
    @cliente = '#menu-item1'
    @dados_cadastrais = '#menu-item1 > ul > li > a'
    @visualizo_dados_cadastrais = '#content > div:nth-child(2) > header > h1'
    @telefone = '#Telefone'
    @msn_telefone = '#content > div:nth-child(2) > div.styles__ToastContainer-sc-1fatpcy-0.hXOybo'
    @email = '#E-mail'
    @cep = '#CEP'
    @num_endereco = '#Numero'
    @complemento_endereco = '#Complemento'
    @profisao = '#Profissão'
    @renda_oculta = '#content > div:nth-child(2) > div:nth-child(5) > div > div > div:nth-child(2) > label'
  end

  def clientes
    find(@cliente).hover
  end

  def dados_cadastrais
    find(@dados_cadastrais).click
  end

  def visualizo_dados_cadastrais
    find(@visualizo_dados_cadastrais)
  end

  def telefone
    page.execute_script 'window.scrollBy(0,10000)'
    find(@telefone).set(convert_phone)
    click_button 'Salvar Telefone'
  end

  def convert_phone
    c = rand(1...20)
    fone = '1196163518'
    fone + c.to_s
  end

  def msn_telefone
    find(@msn_telefone)
  end

  def email
    page.execute_script 'window.scrollBy(0,10000)'
    find(@email).set(convert_email)
    page.execute_script 'window.scrollBy(0,10000)'
    click_button 'Salvar Email'
  end

  def convert_email
    c = rand(1...100_000_000)
    email = 'danilopolicarpo@gmail.com'
    c.to_s + email
  end

  def cep
    page.execute_script 'window.scrollBy(0,1000000)'
    find(@cep).set(convert_cep)
    page.execute_script 'window.scrollBy(0,10000)'
    click_button 'Salvar'
  end

  def convert_cep
    c = rand(0...20)
    cep = '04012-20'
    c.to_s + cep
  end

  def numero_endereco
    page.execute_script 'window.scrollBy(0,1000000)'
    find(@num_endereco).set(convert_numero)
    renda_cliente
    click_button 'Salvar'
  end

  def convert_numero
    c = rand(1...20)
    c.to_s
  end

  def complemento_endereco
    page.execute_script 'window.scrollBy(0,1000000)'
    find(@complemento_endereco).set(convert_cep)
    renda_cliente
    click_button 'Salvar'
  end

  def renda_cliente
    page.execute_script('arguments[0].scrollIntoView();', find(@profisao))
  end

  def ocultar_renda
    element_does_not_exist?(@renda_oculta)
  end
end
