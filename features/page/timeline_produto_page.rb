# frozen_string_literal: true

class Timeline < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/timeline'

  def initialize
    @produto = '.search-page__results-card-brand'
    @timeline = '#content'
    @conta_sidebar = '24130539'
    @cliente_id_sidebar = '23900751'
    @detalhes = '#content > div.sc-jKJlTe.lcKRlK > div:nth-child(2) > section > div.styles__DivValues-sc-1jpbqdr-12.kvAOpw > span'
    @detalhes_informacao = '#content > div.sc-hMqMXs.nVyfy > div.sc-cMljjf.AeDNn > div.sc-jAaTju.cOzblJ > div.sc-jDwBTQ.hXJvUf > div.sc-gPEVay.fHkntr > div.sc-gipzik.eucscH > div:nth-child(2) > div > div'
  end

  def produto_click
    first(@produto).click
  end

  def timeline
    first(@timeline)
  end

  def conta_sidebar
    find('#root > div.sc-kaNhvL.fnzaYs', text: @conta_sidebar)
  end

  def cliente_id_sidebar
    find('#root > div.sc-kaNhvL.fnzaYs', text: @cliente_id_sidebar)
  end

  def detalhes
    find(@detalhes).click
  end

  def detalhes_informacao
    find_button('Reportar problema')
  end

  def encerrar_atendimento
    click_button 'Encerrar atendimento'
  end
end
