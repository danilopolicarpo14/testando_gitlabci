# frozen_string_literal: true

class Fatura < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/statements'

  def initialize
    @menu_fatura = '#menu-item5'
    @fatura_cliente = '#root > div.sc-kaNhvL.fnzaYs > section > div.sc-ipXKqB.gNgUXf > section > div.sc-kLIISr.iEmQsF > section > div'
    @fatura_status = '#root > div.sc-kaNhvL.fnzaYs > section > div.sc-ipXKqB.gNgUXf > section > section > div > div > div > div:nth-child(5) > div > h6:nth-child(1)'
  end

  def menu_fatura
    find(@menu_fatura).click
  end

  def fatura_cliente
    find(@fatura_cliente)
  end

  def fatura_status
    find(@fatura_status)
  end
end