# frozen_string_literal: true

class Contestacao < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'crm/abertura-contestação'

  def initialize
    @tela_contestar = '#content > div:nth-child(2) > header'
    @descricao = 'textarea.form-field.textarea'
    @concordo = '#content > div:nth-child(2) > div.sc-dxgOiQ.fPcmBS'
    @msn_sucesso = '#content > div:nth-child(2) > div.sc-elJkPf.kOQZtV'
    @mercadoria = '#\31  > div > div > div > div:nth-child(2) > label'
    @recebi = '#\31 9 > div > div > div > div:nth-child(1) > label'
    @servico = '#\31  > div > div > div > div:nth-child(3) > label'
    @servico_prestado = '#\32 3 > div > div > div > div:nth-child(1) > label'
  end

  def btn_reportar_problema
    click_button 'Reportar problema'
  end

  def tela_contestar
    find(@tela_contestar)
  end

  def problema_trasancao
    find('label', text: 'Não fiz essa compra').click
  end

  def proximo
    click_button 'Próximo'
  end

  def aconteceu
    find('label', text: 'Não reconheço essa compra')
  end

  def nao_reconheco
    find('label', text: 'Não reconheço essa compra').click
    click_button 'Voltar'
  end

  def opcao_reconheco
    find('label', text: 'Não reconheço essa compra').click
    click_button 'Próximo'
  end

  def escolher_opcoes
    find('label', text: 'Não reconheço essa compra')
  end

  def cartao_roubado
    find('label', text: 'Meu cartão foi roubado/furtado').click
    click_button 'Próximo'
  end

  def mensagem_data
    page.execute_script 'window.scrollBy(0,1000000000)'
    texto = "Quando seu cartão foi roubado/furtado?\nData do ocorrido:"
    page.all('fieldset')[3].text == texto
  end

  def data_today
    d = DateTime.now
    d.strftime '%d/%m/%Y'
  end

  def data_ocorrido
    page.all('fieldset')[3].set data_today
    click_button 'OK'
  end

  def proximo_decricao
    page.all('fieldset')[3]
    click_button 'Próximo'
  end

  def mensagem_ocorrido
    page.all('fieldset')[3]
  end

  def descricao_ocorrido
    page.execute_script 'window.scrollBy(0,10000000)'
    page.all('fieldset')[4].text == 'Descrição do ocorrido:'
  end

  def ocorrido
    page.execute_script 'window.scrollBy(0,10000000)'
    find(@descricao).set 'achei'
    click_button 'Próximo'
  end

  def bloqueio
    page.execute_script 'window.scrollBy(0,10000000)'
    page.all('fieldset')[4].text == 'Bloqueio do cartão'
  end

  def abrir_solicitacao
    click_button 'Abrir solicitação'
  end

  def msn_sucesso
    find_button 'Está bem'
    binding.pry
  end

  def concordo_ocorrido
    page.execute_script 'window.scrollBy(0,100000000)'
    find(@concordo)
  end

  def nao_fiz_compra
    find('label', text: 'Estou com meu cartão mas não fiz essa compra').click
    click_button 'Próximo'
  end

  def problema_mercadoria
    find(@mercadoria).click
    click_button 'Próximo'
  end

  def recebi_mercadoria
    find(@recebi).click
    click_button 'Próximo'
  end

  def ocorrido_mercadoria
    page.execute_script 'window.scrollBy(0,10000000)'
    find(@descricao).set 'teste'
  end

  def msg_abri_solicitacao
   page.execute_script 'window.scrollBy(0,10000000)'
   find_button 'Abrir solicitação'
  end

  def problema_servico
    find(@servico).click
    click_button 'Próximo'
  end

  def servico_contratado
    find(@servico_prestado).click
    click_button 'Próximo'
  end
end
