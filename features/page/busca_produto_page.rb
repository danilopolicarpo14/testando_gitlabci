# frozen_string_literal: true

class Busca < SitePrism::Page
  set_url ENVIRONMENT['CB'] + 'search'

  def initialize
    @cpf = 'input#input-search'
    @mensagem_erro = 'Nenhum resultado encontrado'
    @produto = '.search-page__results-card-brand'
  end

  def buscar_cpf(arg)
    id = PRODUTO[arg.tr(' ', '_').to_sym][:cpf]
    first(@cpf).set id
    click_button 'Buscar'
  end

  def produto_cliente
    first(@produto)
  end

  def mensagem_erro
    find('search-container', text: @mensagem_erro)
  end
end
