#language:pt

Funcionalidade: Tickets
Aguardando definição do PO(Conversa com Ueda e Jesus)

@prod
Cenário: Visualizar tickets na plataforma CRM
Dado que estou na timeline do cliente
Quando clicar em Historico
E Historico de atendimento
Então vejo tickets do atendimento do cliente

Cenário: Mensagens para clientes sem tickets na plataforma CRM
Dado que estou na timeline com cliente sem tickets
Quando clicar em Historico
E Historico de atendimento
Então vejo a mensagem não há tickets