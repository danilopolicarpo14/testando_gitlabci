#language:pt

Funcionalidade: Abertura da contestação
Exibição do formulário (guidance) onde o cliente preenche a contestação para compras nacionais e internacionais

Cenário: Visualizar Solicitação de Contestação na plataforma CRM
Dado que estou na timeline do cliente abertura de contestacao
E visualizo detalhes da compra do cliente
Quando clicar em Reportar problema
Então vejo a tela de Solicitação de abertura de Contestação

Cenário: Visualizar Qual foi o problema com essa transação?
Dado que estou na tela de Contestação
Quando informo que não fiz essa compra
E clicar em próximo
Então vejo a apção o que aconteceu?

Cenário: Voltar para opção anterior
Dado que estou na tela de Contestação
Quando informo que não fiz essa compra
E clicar em próximo
Então vejo opção voltar

Cenário: Visualizar as opções Qual das situações a baixo se enquadram melhor
Dado que estou na tela qual foi o problema com essa transação?
Quando informo que não reconheço a compra
Então vejo as opções em Qual das situações a baixo se enquadram melhor

Cenário: Posso escolher o motivo meu cartão foi roubado/furtado?
Dado que estou nas opções em Qual das situações a baixo se enquadram melhor
Quando informo a opção meu cartão foi roubado / furtado
Então vejo a mensagem Quando seu cartão foi roubado ou furtado?
E data do ocorrido

Cenário: Ao informar a data do ocorrido vejo o opção para descrever o motivo
Dado que estou na opção Quando seu cartão foi roubado / furtado?
Quando informo a opção  Data do ocorrido
E clicar no próximo
Então vejo opção para descrever o motivo de roubo/furto

Cenário: Descrever o motivo do ocorrido da abertura de contestação
Dado que estou na tela descrever o motivo da contestação
Quando informo a descrição do ocorrido
Então vejo a mensagem Bloqueio do cartão

@mock
Cenário: Solicitar abertura de Contestação
Dado que estou na opção Bloqueio do cartão
Quando Visualizar a opção Concordo com o bloqueio do cartão
E clicar em Abrir Solicitação
Então vejo mensagem de sucesso de Abertura de Contestação

@mock
Cenário: Solicitar abertura de Contestação para estou com meu cartão mas não fiz essa compra
Dado que estou na opção Bloqueio do cartão mas não fiz essa compra
Quando Visualizar a opção Concordo com o bloqueio do cartão
E clicar em Abrir Solicitação
Então vejo mensagem de sucesso de Abertura de Contestação

Cenário: Visualizar Qual foi o problema com essa transação para a opção problema com a mercadoria
Dado que estou na tela qual foi o problema com essa transação opção problema com a mercadoria 
Quando informo que tive um problema com uma mercadoria
Então vejo as opções em Qual das situações a baixo se enquadram melhor para opção mercadoria

Cenário: Posso escolher a opção não recebi a mercadoria 
Dado que vejo a opção tive um problema com uma mercadoria
Quando informo a descrição do ocorrido para opção mercadoria
Então vejo a opção clicar em Abrir Solicitação

Cenário: Visualizar Qual foi o problema com essa transação para serviço contratado
Dado que estou na tela qual foi o problema com essa transação opção serviço contratado
Quando informo que tive um problema com um serviço contratado
Então vejo a tela O serviço foi prestado?

Cenário: Posso escolher um opção o serviço contratado
Dado que vejo a opção tive um problema com serviço contratado
Quando informo a descrição do ocorrido para opção mercadoria
Então vejo a opção clicar em Abrir Solicitação
