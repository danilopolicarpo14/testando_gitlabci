#language:pt

Funcionalidade: Fatura do Cliente
Acompanhamento do cliente dos status das 
faturas (paga, fechada, aberta, atrasada, próxima fatura)

Cenário: Acessar a Fatura do cliente
Dado que estou na timeline do cliente
Quando clicar em Fatura
Então vejo as faturas do cliente

Cenário: Verificar o status da Fatura
Dado que estou na timeline do cliente
Quando clicar em Fatura
Então visualizo o status da fatura
