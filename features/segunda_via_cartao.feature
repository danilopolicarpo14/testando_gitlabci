#language:pt

Funcionalidade: Solitação 2ª via do Cartão
Manter o cliente ativo utilizando todos os recursos na plataforma

Por razões de segurança e fraudes não será possível fazer o envio da 2ª via,
Consulte o serviço de solicitação de 2ª via no seu app. Quando o endereço 
cadastrado foi alterado recentemente.

Cliente que tiver uma solicitação feita via app ou CRM nos últimos 30 dias:
- Não será possível emitir uma 2a via. O usuário será informado via modal.


Cenário: Visualizar a solicitação da 2ª via do cartão
Dado que estou na timeline do cliente
Quando clicar em Cartão
E solicitação de 2ª via do cartão
Então vejo tela de Solicitação de 2ª via do cartão

Cenário: Visualizar a mensagem de bloqueio do cartão 
Dado que estou na tela de Solitação de 2ª do cartão
Quando informar o motivo da solicitação
E clicar em solicitar 2ª via
Então visualizo a mensagem de bloqueio do cartão

Cenário: Visualizar a mensagem de Serviços e Assinaturas
Dado que estou na tela de bloqueio do cartão
Quando clicar em seguir
Então visualizo a mensagem de Serviços e Assinaturas

Cenário: Visualizar a mensagem de Previsão de Entregas
Dado que estou na tela de Serviços e Assinaturas
Quando clicar em seguir
Então visualizo a mensagem de Previsão de Entregas

Cenário: Visualizar a mensagem de Confirmar a emissão de 2ª via
Dado que estou na tela de Previsão de Entregas
Quando clicar em seguir
Então visualizo a mensagem de Confirmar a emissão de 2ª via

Cenário: Cancelar a emissao de 2ª via do cartão
Dado que estou na tela Confirmar a emissão de 2ª via
Quando cancelar a emissão da 2ª via
Então vejo tela de Solitação de 2ª via do cartão

Cenário: Prosseguir com a emissao de 2ª via do cartão
Dado que estou na tela Confirmar a emissão de 2ª via
Quando clicar em sim para a emissão da 2ª via
Então vejo informação de nova senha gerada 

#aguardando o fluxo de gerar nova senha
# Cenário: Posso gerar nova senha antes de pedir a 2ª via do cartão
# Dado que estou na tela de confirmação Deseja manter a senha atual ?
# Quando clicar em Gerar nova senha
# Então aguardando o fluxo de gerar nova senha

@mock
Cenário: Realizar o pedido de 2ª via do cartão
Dado que estou na tela de atualização da senha
Quando clicar ok
Então vejo a mensagem 2ª via emitida com sucesso