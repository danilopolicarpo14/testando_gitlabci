# frozen_string_literal: true

require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'site_prism'
require 'pry'
require 'date'
require_relative 'page_helper.rb'


World(Pages)

ENVIRONMENT = YAML.load_file('./config/env.yml')[ENV['PLATFORM']]

Capybara.register_driver :selenium_chrome_headless do |app|
  Capybara::Selenium::Driver.load_selenium
  browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
    opts.args << '--headless'
    opts.args << '--disable-gpu' if Gem.win_platform?
    opts.args << '--no-sandbox'
    opts.args << '--disable-site-isolation-trials'
    opts.args << '--enable-features=NetworkService,NetworkServiceInProcess'
  end
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
end

Capybara.configure do |config|
  config.default_driver = :selenium_chrome_headless
  config.app_host = ENVIRONMENT['CB']
  config.default_max_wait_time = 8
end

