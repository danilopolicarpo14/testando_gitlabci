#language:pt

Funcionalidade: Login 
Como usuário da plataforma-CB, acesso o CRM
para ajudar e verificar informaçoes de clientes

@prod
Cenário: Realizar Login com sucesso
Dado que estou na tela de login
Quando realizar login com credenciais "válidas"
Então visualizo a tela de busca

Cenário: Usuário sem acesso a plataforma
Dado que estou na tela de login
Quando realizar login com credenciais "sem acesso"
Então visualizo mensagem de erro