# frozen_string_literal: true

Quando('detalhes de cartões') do
  @cartao = Cartao.new
  @cartao.detalhes_cartoes
end

Então('vejo detalhes de cartões do atendimento do cliente') do
  @cartao = Cartao.new
  @cartao.page_detalhes
end

Então('vejo o nome do produto pertencente ao cartão fisico') do
  @cartao = Cartao.new
  @cartao.nome_produto
end

Então('vejo a previsão de entrega para o cartão fisico') do
  @cartao = Cartao.new
  @cartao.previsao_entrega
end

Então('vejo a informação da bandeira do cartão fisico') do
  @cartao = Cartao.new
  @cartao.bandeira
end

Então('vejo a informação do status do cartão') do
  @cartao = Cartao.new
  @cartao.status
end
