# frozen_string_literal: true

Dado('que estou na timeline com cliente sem tickets') do
  steps %(
    Dado que estou na tela de buscar
    Quando buscar o CPF do cliente "sem_tickets"
    E clicar no produto do cliente
    Então visualizo a timeline com as informações do cliente
  )
end

Quando('clicar em Historico') do
  @tickets = Tickets.new
  @tickets.menu_historico
end

Quando('Historico de atendimento') do
  @tickets = Tickets.new
  @tickets.historico_atendimento
end

Então('vejo tickets do atendimento do cliente') do
  @tickets = Tickets.new
  @tickets.tela_tickets
end

Então('vejo a mensagem não há tickets') do
  @tickets = Tickets.new
  @tickets.msn_tickets
end
