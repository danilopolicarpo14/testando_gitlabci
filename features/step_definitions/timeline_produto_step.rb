# frozen_string_literal: true

Quando('clicar no produto do cliente') do
  @line = Timeline.new
  @line.produto_click
end

Então('visualizo a timeline com as informações do cliente') do
  @line = Timeline.new
  @line.timeline
end

Então('vejo as informações da conta e identificador do cliente') do
  @line = Timeline.new
  @line.conta_sidebar
  @line.cliente_id_sidebar
end

Dado('que estou na timeline do cliente') do
  steps %(
    Dado que estou na tela de buscar
    Quando buscar o CPF do cliente "cadastrado"
    E clicar no produto do cliente
    Então visualizo a timeline com as informações do cliente
  )
end

Dado('que estou na timeline do cliente abertura de contestacao') do
  steps %(
    Dado que estou na tela de buscar
    Quando buscar o CPF do cliente "conta_ativa_contestação"
    E clicar no produto do cliente
    Então visualizo a timeline com as informações do cliente
  )
end

Quando('clicar no detalhes da compra') do
  @line = Timeline.new
  @line.detalhes
end

Entao('vejo as informações referentes a compra do cliente') do
  @line = Timeline.new
  @line.detalhes_informacao
end

Quando('clicar em encerrar atendimento') do
  @line = Timeline.new
  @line.encerrar_atendimento
end
