# frozen_string_literal: true

Dado('que estou na tela de buscar') do
  steps %(
    Dado que estou na tela de login
    Quando realizar login com credenciais "válidas"
    Entao visualizo a tela de busca
  )
end

Quando('buscar o CPF do cliente {string}') do |string|
  @busca = Busca.new
  @busca.buscar_cpf string
end

Então('apresentar o produto do cliente') do
  @busca = Busca.new
  @busca.produto_cliente
end

Então('visualizo mensagem de erro na busca') do
  @busca = Busca.new
  @busca.mensagem_erro
end

Então('apresentar os produtos do cliente') do
  @busca = Busca.new
  @busca.produto_cliente
end
