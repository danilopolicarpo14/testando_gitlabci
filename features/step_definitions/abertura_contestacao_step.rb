# frozen_string_literal: true

Dado('visualizo detalhes da compra do cliente') do
  steps %(
    Quando clicar no detalhes da compra
    Entao vejo as informações referentes a compra do cliente
  )
end

Quando('clicar em Reportar problema') do
  @contestacao = Contestacao.new
  @contestacao.btn_reportar_problema
end

Então('vejo a tela de Solicitação de abertura de Contestação') do
  @contestacao = Contestacao.new
  @contestacao.tela_contestar
end

Dado('que estou na tela de Contestação') do
  steps %(
  Dado que estou na timeline do cliente abertura de contestacao
  E visualizo detalhes da compra do cliente
  Quando clicar em Reportar problema
  Então vejo a tela de Solicitação de abertura de Contestação
  )
end

Quando('informo que não fiz essa compra') do
  @contestacao = Contestacao.new
  @contestacao.problema_trasancao
end

E('clicar em próximo') do
  @contestacao = Contestacao.new
  @contestacao.proximo
end

Então('vejo a apção o que aconteceu?') do
  @contestacao = Contestacao.new
  @contestacao.aconteceu
end

Então('vejo opção voltar') do
  @contestacao = Contestacao.new
  @contestacao.nao_reconheco
end

Dado('que estou na tela qual foi o problema com essa transação?') do
  steps %(
  Dado que estou na tela de Contestação
  Quando informo que não fiz essa compra
  E clicar em próximo
  Então vejo a apção o que aconteceu?
  )
end

Quando('informo que não reconheço a compra') do
  @contestacao = Contestacao.new
  @contestacao.opcao_reconheco
end

Então('vejo as opções em Qual das situações a baixo se enquadram melhor') do
  @contestacao = Contestacao.new
  @contestacao.escolher_opcoes
end

Dado('que estou nas opções em Qual das situações a baixo se enquadram melhor') do
  steps %(
  Dado que estou na tela qual foi o problema com essa transação?
  Quando informo que não reconheço a compra
  Então vejo as opções em Qual das situações a baixo se enquadram melhor
  )
end

Quando('informo a opção meu cartão foi roubado / furtado') do
  @contestacao = Contestacao.new
  @contestacao.cartao_roubado
end

Então('vejo a mensagem Quando seu cartão foi roubado ou furtado?') do
  @contestacao = Contestacao.new
  @contestacao.mensagem_data
end

E('data do ocorrido') do
  @contestacao = Contestacao.new
  @contestacao.data_ocorrido
end

Dado("que estou na opção Quando seu cartão foi roubado \/ furtado?") do
  steps %(
  Dado que estou nas opções em Qual das situações a baixo se enquadram melhor
  Quando informo a opção meu cartão foi roubado / furtado
  Então vejo a mensagem Quando seu cartão foi roubado ou furtado?
  )
end

Quando('informo a opção  Data do ocorrido') do
  @contestacao = Contestacao.new
  @contestacao.data_ocorrido
end

E('clicar no próximo') do
  @contestacao = Contestacao.new
  @contestacao.proximo_decricao
end

Então('vejo opção para descrever o motivo de roubo\/furto') do
  @contestacao = Contestacao.new
  @contestacao.mensagem_ocorrido
end

Dado('que estou na tela descrever o motivo da contestação') do
  steps %(
  Dado que estou na opção Quando seu cartão foi roubado / furtado?
  Quando informo a opção  Data do ocorrido
  E clicar no próximo
  Então vejo opção para descrever o motivo de roubo/furto
)
end

Quando('informo a descrição do ocorrido') do
  @contestacao = Contestacao.new
  @contestacao.ocorrido
end

Então('vejo a mensagem Bloqueio do cartão') do
  @contestacao = Contestacao.new
  @contestacao.bloqueio
end

Dado('que estou na opção Bloqueio do cartão') do
  steps %(
  Dado que estou na tela descrever o motivo da contestação
  Quando informo a descrição do ocorrido
  Então vejo a mensagem Bloqueio do cartão
  )
end

Quando('Visualizar a opção Concordo com o bloqueio do cartão') do
  @contestacao = Contestacao.new
  @contestacao.concordo_ocorrido
end

Quando('clicar em Abrir Solicitação') do
  @contestacao = Contestacao.new
  @contestacao.abrir_solicitacao
end

Então('vejo mensagem de sucesso de Abertura de Contestação') do
  @contestacao = Contestacao.new
  @contestacao.msn_sucesso
end

Quando('escolho a opção estou meu cartao mas não fiz essa compra') do
  @contestacao = Contestacao.new
  @contestacao.nao_fiz_compra
end

Dado('que estou na opção Bloqueio do cartão mas não fiz essa compra') do
  steps %(
  Dado que estou na tela qual foi o problema com essa transação?
  E informo que não reconheço a compra
  Quando vejo as opções em Qual das situações a baixo se enquadram melhor
  E escolho a opção estou meu cartao mas não fiz essa compra
  Então informo a descrição do ocorrido
  )
end

Dado('que estou na tela qual foi o problema com essa transação opção problema com a mercadoria') do
  steps %(
  Dado que estou na tela de Contestação
  )
end

Quando('informo que tive um problema com uma mercadoria') do
  @contestacao = Contestacao.new
  @contestacao.problema_mercadoria
end

Então('vejo as opções em Qual das situações a baixo se enquadram melhor para opção mercadoria') do
  @contestacao = Contestacao.new
  @contestacao.recebi_mercadoria
end

Dado('que vejo a opção tive um problema com uma mercadoria') do
  steps %(
  Dado que estou na tela qual foi o problema com essa transação opção problema com a mercadoria
  Quando informo que tive um problema com uma mercadoria
  Então vejo as opções em Qual das situações a baixo se enquadram melhor para opção mercadoria
  )
end

Quando('informo a descrição do ocorrido para opção mercadoria') do
  @contestacao = Contestacao.new
  @contestacao.ocorrido_mercadoria
end

Então("vejo a opção clicar em Abrir Solicitação") do
  @contestacao = Contestacao.new
  @contestacao.msg_abri_solicitacao
end

Dado('que estou na tela qual foi o problema com essa transação opção serviço contratado') do
  steps %(
  Dado que estou na tela de Contestação
  )
end

Quando('informo que tive um problema com um serviço contratado') do
  @contestacao = Contestacao.new
  @contestacao.problema_servico
end

Então('vejo a tela O serviço foi prestado?') do
  @contestacao = Contestacao.new
  @contestacao.servico_contratado
end


Dado('que vejo a tela Descrever o motivo do ocorrido') do
  steps %(
  Dado que estou na opção tive um problema com um serviço prestado
  Quando vejo a tela O serviço foi prestado?
  E informo a descrição do ocorrido para opção mercadoria
  Então("vejo a opção clicar em Abrir Solicitação")

  )
end

Dado('que vejo a opção tive um problema com serviço contratado') do
  steps %(
  Dado que estou na tela qual foi o problema com essa transação opção serviço contratado
  Quando informo que tive um problema com um serviço contratado
  Então vejo a tela O serviço foi prestado?
  E informo a descrição do ocorrido para opção mercadoria
  Então vejo a opção clicar em Abrir Solicitação 
  )
end