# frozen_string_literal: true

Dado('que estou na tela de login') do
  login_page.load
end

Quando('realizar login com credenciais {string}') do |string|
  @login = Login.new
  @login.realizar_login string
end

Então('visualizo mensagem de erro') do
  @login = Login.new
  @login.mensagem_erro
end

Entao('visualizo a tela de busca') do
  @login = Login.new
  @login.tela_buscar
end
