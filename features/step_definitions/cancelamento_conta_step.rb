# frozen_string_literal: true

Dado('que estou na timeline com uma conta ativa') do
  steps %(
    Dado que estou na tela de buscar
    Quando buscar o CPF do cliente "conta_ativa"
    E clicar no produto do cliente
    Então visualizo a timeline com as informações do cliente
  )
end

Quando('clicar em Conta + Cancelamento de conta') do
  @delete = Cancel.new
  @delete.cancelamento_conta
end

Então('vejo tela de Cancelamento de Conta') do
  @delete = Cancel.new
  @delete.tela_cancelamento_conta
end

Dado('que estou na tela Cancelamento de Conta') do
  steps %(
    Dado que estou na timeline com uma conta ativa
    Quando clicar em Conta + Cancelamento de conta
    Então vejo tela de Cancelamento de Conta
  )
end

Quando('informar o motivo do Cancelamento da Conta sem descrição') do
  @delete = Cancel.new
  @delete.motivo_cancelamento
end

Então('vejo mensagens importantes sobre o cancelamento da conta') do
  @delete = Cancel.new
  @delete.mensagem_cancelamento
end

Quando('informar o motivo do Cancelamento da Conta') do
  @delete = Cancel.new
  @delete.motivo_cancelamento_com_descricao
end

Quando('informar uma descrição') do
  @delete = Cancel.new
  @delete.informar_descricao
end

Dado('que estou na timeline com uma conta Cancelada') do
  steps %(
    Dado que estou na timeline do cliente
  )
end

Quando('realizar o cancelamento da conta') do
  steps %(
    Quando clicar em Conta + Cancelamento de conta
  )
end

Então('vejo mensagem informando que a conta do cliente ja foi Cancelada') do
  @delete = Cancel.new
  @delete.conta_cancelada
end

# cancelamento de conta

Dado('que estou na timeline com uma conta {string}') do |string|
  steps %(
    Dado que estou na tela de buscar
  )
  @delete = Cancel.new
  @delete.buscar_cpf string
  @delete.produto_cliente
end

Quando('realizar o Cancelamento da Conta') do
  steps %(
    Dado clicar em Conta + Cancelamento de conta
    Quando informar o motivo do Cancelamento da Conta
    E informar uma descrição
  )
  @delete = Cancel.new
  @delete.cancelar_conta
end

Então('vejo mensagem da conta Cancelada com sucesso') do
  @delete = Cancel.new
  @delete.msn_cancelamento
end

# fim do cancelamento de conta
