# frozen_string_literal: true

Quando('clicar em Cartão') do
  @segunda = SegundaVia.new
  @segunda.cartao
end

Quando('solicitação de {int}ª via do cartão') do |_int|
  @segunda = SegundaVia.new
  @segunda.segunda_via
end

Então('vejo tela de Solicitação de {int}ª via do cartão') do |_int|
  @segunda = SegundaVia.new
  @segunda.tela_segunda_via
end

Dado('que estou na tela de Solitação de {int}ª do cartão') do |_int|
  steps %(
    Dado que estou na timeline do cliente
    Quando clicar em Cartão
    E solicitação de 2ª via do cartão
    Então vejo tela de Solicitação de 2ª via do cartão
  )
end

Quando('informar o motivo da solicitação') do
  @segunda = SegundaVia.new
  @segunda.motivo_segunda_via
end

Quando('clicar em solicitar {int}ª via') do |_int|
  @segunda = SegundaVia.new
  @segunda.btn_solicitar_segunda_via
end

Então('visualizo a mensagem de bloqueio do cartão') do
  @segunda = SegundaVia.new
  @segunda.msn_bloqueio_cartao
end

Dado('que estou na tela de bloqueio do cartão') do
  steps %(
    Dado que estou na tela de Solitação de 2ª do cartão
    Quando informar o motivo da solicitação
    E clicar em solicitar 2ª via
    Então visualizo a mensagem de bloqueio do cartão
  )
end

Quando('clicar em seguir') do
  @segunda = SegundaVia.new
  @segunda.btn_proximo
end

Então('visualizo a mensagem de Serviços e Assinaturas') do
  @segunda = SegundaVia.new
  @segunda.servico_assinatura
end

Dado('que estou na tela de Serviços e Assinaturas') do
  steps %(
    Dado que estou na tela de bloqueio do cartão
    Quando clicar em seguir
    Então visualizo a mensagem de Serviços e Assinaturas
  )
end

Então('visualizo a mensagem de Previsão de Entregas') do
  @segunda = SegundaVia.new
  @segunda.previsao_entrega
end

Dado('que estou na tela de Previsão de Entregas') do
  steps %(
    Dado que estou na tela de Serviços e Assinaturas
    Quando clicar em seguir
    Então visualizo a mensagem de Previsão de Entregas
  )
end

Então('visualizo a mensagem de Confirmar a emissão de {int}ª via') do |_int|
  @segunda = SegundaVia.new
  @segunda.confirmar_emissao
end

Dado('que estou na tela Confirmar a emissão de {int}ª via') do |_int|
  steps %(
    Dado que estou na tela de Previsão de Entregas
    Quando clicar em seguir
    Então visualizo a mensagem de Confirmar a emissão de 2ª via
  )
end

Quando('cancelar a emissão da {int}ª via') do |_int|
  @segunda = SegundaVia.new
  @segunda.btn_sair
end

Então('vejo tela de Solitação de {int}ª via do cartão') do |_int|
  steps %(
    Então vejo tela de Solicitação de 2ª via do cartão
  )
end

Quando('clicar em sim para a emissão da {int}ª via') do |_int|
  @segunda = SegundaVia.new
  @segunda.btn_sim
end

Então('vejo informação de nova senha gerada') do
end
