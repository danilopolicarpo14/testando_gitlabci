# frozen_string_literal: true

Quando('clicar em cliente') do
  @dados = Cadastro.new
  @dados.clientes
end

Quando('dados cadastrais') do
  @dados = Cadastro.new
  @dados.dados_cadastrais
end

Então('visualizo os dados do cliente') do
  @dados = Cadastro.new
  @dados.visualizo_dados_cadastrais
end

Dado('que estou na tela de Dados Cadastrais') do
  steps %(
    Dado que estou na timeline do cliente
    Quando clicar em cliente
    E dados cadastrais
    Então visualizo os dados do cliente
  )
end

Quando('alterar o numero do Telefone') do
  @dados = Cadastro.new
  @dados.telefone
end

Então('vejo mensagem de sucesso') do
  @dados = Cadastro.new
  @dados.msn_telefone
end

Quando('alterar o e-mail') do
  @dados = Cadastro.new
  @dados.email
end

Quando('alterar o CEP') do
  @dados = Cadastro.new
  @dados.cep
end

Quando('alterar o numero do endereço') do
  @dados = Cadastro.new
  @dados.numero_endereco
end

Quando('alterar o complemento do endereço') do
  @dados = Cadastro.new
  @dados.complemento_endereco
end

Quando('arrastar para baixo até Informação Profissionais') do
  @dados = Cadastro.new
  @dados.renda_cliente
end

Então('vejo oculto o valor da renda do profissional') do
  @dados = Cadastro.new
  @dados.ocultar_renda
end
