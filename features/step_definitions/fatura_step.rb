# frozen_string_literal: true

Quando('clicar em Fatura') do
  @fatura = Fatura.new
  @fatura.menu_fatura
end

Então('vejo as faturas do cliente') do
  @fatura = Fatura.new
  @fatura.fatura_cliente
end

Então('visualizo o status da fatura') do
  @fatura = Fatura.new
  @fatura.fatura_status
end
