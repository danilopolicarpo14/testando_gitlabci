#language:pt

Funcionalidade: Cartão
Informações referente ao cartão do usuário 
como por exemplo: previsão de entrega, informação da bandeira,
cartão virtual recorrente, cartão virtual 24 horas com seus status 
ativo,cancelados,roubado ou desbloqueado.

@prod
Cenário: Visualizar cartão na plataforma CRM
Dado que estou na timeline do cliente
Quando clicar em Cartão
E detalhes de cartões
Então vejo detalhes de cartões do atendimento do cliente

Cenário: Visualizar o nome do produto pertencente ao cartão fisico
Dado que estou na timeline do cliente
Quando clicar em Cartão
E detalhes de cartões
Então vejo o nome do produto pertencente ao cartão fisico

Cenário: Visualizar a previsão de entrega para o cartão fisico
Dado que estou na timeline do cliente
Quando clicar em Cartão
E detalhes de cartões
Então vejo a previsão de entrega para o cartão fisico

Cenário: Visualizar a informação da bandeira do cartão fisico
Dado que estou na timeline do cliente
Quando clicar em Cartão
E detalhes de cartões
Então vejo a informação da bandeira do cartão fisico

Cenário: Visualizar a informação do status do cartão
Dado que estou na timeline do cliente
Quando clicar em Cartão
E detalhes de cartões
Então vejo a informação do status do cartão