#language:pt

Funcionalidade: Dados Cadastrais
Posso alterar alguns dados do cliente via plataforma CRM 
(telefone,e-mail,endereço do cliente)

@prod
Cenário: Visualizar Dados Cadastrais na plataforma CRM
Dado que estou na timeline do cliente
Quando clicar em cliente
E dados cadastrais 
Então visualizo os dados do cliente

Cenário: Posso alterar com sucesso o número de Telefone do cliente
Dado que estou na tela de Dados Cadastrais
Quando alterar o numero do Telefone
Então vejo mensagem de sucesso 

Cenário: Posso alterar com sucesso o e-mail do cliente
Dado que estou na tela de Dados Cadastrais
Quando alterar o e-mail 
Então vejo mensagem de sucesso 

Cenário: Posso alterar com sucesso o CEP do cliente
Dado que estou na tela de Dados Cadastrais
Quando alterar o CEP
Então vejo mensagem de sucesso 

Cenário: Posso alterar com sucesso o numero do endereço do cliente
Dado que estou na tela de Dados Cadastrais
Quando alterar o numero do endereço
Então vejo mensagem de sucesso

Cenário: Posso alterar com sucesso o complemento do endereço do cliente
Dado que estou na tela de Dados Cadastrais
Quando alterar o complemento do endereço
Então vejo mensagem de sucesso

Cenário: Ocultar renda na página de Dados do Cliente
Dado que estou na tela de Dados Cadastrais
Quando arrastar para baixo até Informação Profissionais
Então vejo oculto o valor da renda do profissional