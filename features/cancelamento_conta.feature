#language:pt

Funcionalidade: Cancelamento de conta
O atendente pode cancelar uma conta pertencente ao produto do usuário vinculado ao CPF
atualmente na plataforma temos os seguintes produtos:
Cartão Branco;
Credicard Beta;
Mercado livre (produto cancelado).

Cenário: Visualizar Cancelamento de Conta na plataforma CRM
Dado que estou na timeline com uma conta ativa
Quando clicar em Conta + Cancelamento de conta 
Então vejo tela de Cancelamento de Conta

Cenário: Posso Cancelar uma conta ativa sem informar a descrição
Dado que estou na tela Cancelamento de Conta
Quando informar o motivo do Cancelamento da Conta sem descrição
Então vejo mensagens importantes sobre o cancelamento da conta

Cenário: Posso Cancelar uma conta ativa  informando uma descrição
Dado que estou na tela Cancelamento de Conta
Quando informar o motivo do Cancelamento da Conta 
E informar uma descrição
Então vejo mensagens importantes sobre o cancelamento da conta

Cenário: Realizar um Cancelamento de uma conta com status de Cancelada na timeline
Dado que estou na timeline com uma conta Cancelada
Quando realizar o cancelamento da conta
Então vejo mensagem informando que a conta do cliente ja foi Cancelada

@mock
Cenário: Realizar um Cancelamento de Conta na plataforma CRM
Dado que estou na timeline com uma conta ativa
Quando realizar o Cancelamento da Conta
Então vejo mensagem da conta Cancelada com sucesso

# @mock
# Esquema do Cenário: Realizar um Cancelamento de Conta mercado livre na plataforma CRM
# Dado que estou na timeline com uma conta "<Mercado_livre>"
# Quando realizar o Cancelamento da Conta
# Então vejo mensagem da conta Cancelada com sucesso

# Exemplos:
# |Mercado_livre |
#siraldo
#|33160004880|

#conta dev 
#|67053182444|

# CPF com dois produtos 
#raphael  |123.511.107-57|
#fernando |378.229.108-50|
#jenifer  |044.599.253-03|
