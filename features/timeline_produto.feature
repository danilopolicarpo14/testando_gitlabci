#language:pt

Funcionalidade: Timeline
Como atendente logado visualizo as informações referentes 
as atividades que o cliente 
fez, compras aprovadas,negadas e fechamento de fatura

@prod
Cenário: Visualizo a timeline do cliente
Dado que estou na tela de buscar
Quando buscar o CPF do cliente "cadastrado"
E clicar no produto do cliente
Então visualizo a timeline com as informações do cliente

Cenário: Visualizar a informação da conta e identificador do cliente na timeline 
Dado que estou na tela de buscar
Quando buscar o CPF do cliente "cadastrado"
E clicar no produto do cliente
Então vejo as informações da conta e identificador do cliente

Cenário: Visualizar detalhes da compra do cliente na Timeline
Dado que estou na timeline do cliente
Quando clicar no detalhes da compra
Então vejo as informações referentes a compra do cliente

Cenário: Encerrar atendimento na Timeline
Dado que estou na timeline do cliente
Quando clicar em encerrar atendimento
Então visualizo a tela de busca