#language:pt

Funcionalidade: Busca por CPF 
Como atendente logado e possivel atraves do CPF buscar o produto do cliente

@prod
Cenário: Busca por CPF cadastrado
Dado que estou na tela de buscar
Quando buscar o CPF do cliente "cadastrado"
Então apresentar o produto do cliente

@mock
Cenário: Busca por CPF cadastrado com mais de um produto
Dado que estou na tela de buscar
Quando buscar o CPF do cliente "cadastrado_mais_de_um_produto"
Então apresentar os produtos do cliente 

Cenário: Busca por CPF não cadastrado
Dado que estou na tela de buscar
Quando buscar o CPF do cliente "não cadastrado"
Então visualizo mensagem de erro na busca